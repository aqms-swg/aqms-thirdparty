# Third party software required for building and running AQMS  
  
## OS dependencies  
  
RHEL/CentOS 7 packages for a 64-bit installation.   

```
yum -y install compat-libcap1 compat-libstdc++-33 gcc gcc-c++ gcc-gfortran   \
    glibc glibc-devel imake libaio libaio-devel libconfig libconfig-devel    \
    libgfortran libpcap libpcap-devel libstdc++ libstdc++-devel libtool-ltdl \
    perl-devel perl-Data-Dumper subversion
```  

  
## ACE-TAO  
  
Adaptive Communication Environment (ACE) is an object-oriented (OO) toolkit that implements fundamental design patterns for
communication software.  ACE provides a rich set of reusable C++ wrappers and frameworks that perform common communication software
tasks across a range of OS platforms. For details see https://www.dre.vanderbilt.edu/~schmidt/ACE.html  

The ACE ORB (TAO) is an open source C++ implementation of the Object Management Group (OMG) Common Object Request Broker Architecture (CORBA) version 3.0. TAO is supported by OCI. For details see https://theaceorb.com/    

Download the OCI ACE+TAO distribution As of this writing, **the current version is 2.2a**. (NOTE: You must use OCI's redistribution of the ACE ORB. Other distributions will not work).      

```    
wget http://download.ociweb.com/TAO-2.2a/ACE+TAO-2.2a_with_latest_patches.tar.gz  
```
  
  
## Comserv2  
  
Used to build cs2ew, cs2rwp, cs2wda, etc. Source code is in an SVN repository maintained by ISTI.  

```
svn export svn://svn.isti.com/comserv2/dev comserv2
```
  
  
##  qlib2  
  
```
wget http://www.ncedc.org/qug/software/ucb/qlib2.2019.365.tar.gz
tar -zxvf qlib2.2019.365.tar.gz
```
  

## OTL  
  
```
mkdir OTL    # <-- This MUST be uppercase

wget http://otl.sourceforge.net/otlv4_h2.zip
unzip otlv4_h2.zip
```
  

## Xerces-C++  
  
Xerces-C++ is a validating XML parser written in a portable subset of C++.  
  

## Oracle instant client  


## Postgres  



## Earthworm (EW)  

Current version : 7.9

**Important:** In order to generate the libew.so library for AQMS, EW's libew.a file must be compiled using -fPIC. You can check this by extracting the object files from libew.a and checking their relocations (see below). If the output is empty, then the static library is not position-independent and cannot be used to generate a shared object.
  
```
ar -x libew.a  
readelf --relocs chron3.o | egrep '(GOT|PLT|JU?MP_SLOT)'
```
  
```
wget http://www.earthwormcentral.org/distribution/earthworm_7.9-src.tar.gz
tar -zxvf earthworm_7.9-src.tar.gz
```
  


